# README #

### What is this repository for? ###

* This is a repository for test task from Iftach for SimmilarWeb
* See last in [changelog](changelog.md)

### How do I get set up? ###

* cd server/
* npm install
* npm start

Then

* install extention to chrome from /ext folder
* visit a few pages or change url a few times
* go to [http://localhost:3000/history](http://localhost:3000/history) or click on extention tooltip button