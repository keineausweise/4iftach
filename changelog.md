### 0.1.2
- added onClick handler for extention button

### 0.1.1
- correct data sorting (from new to old)
- fixed time diff for seconds

### 0.1.0
- server part done
- web face done
- extention written

### 0.0.1
- Initial commit