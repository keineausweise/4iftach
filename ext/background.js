chrome.browserAction.onClicked.addListener(function(activeTab)
{
    var newURL = "http://localhost:3000/history";
    chrome.tabs.create({ url: newURL });
});

chrome.tabs.onUpdated.addListener(function(tabId, update, tab) {
    if (update.status === "complete"){
        postIntel(tab.url);
    }
});

function postIntel(url){
    var hr = new XMLHttpRequest();

    var postUrl = "http://localhost:3000/api/store";
    var postData = JSON.stringify({
        href: url
    });
    hr.open("POST", postUrl, true);

    hr.setRequestHeader("Content-type", "application/json");

    hr.send(postData);
}