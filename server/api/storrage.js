var jsonfile = require('jsonfile');
var timediff = require('timediff');
Object.assign = Object.assign || require('object-assign');

var JSON_OPTIONS = {
    encoding: 'utf8'
};

var memCache = [];

var _filePath;

function init(filePath){
    _filePath = filePath;
    memCache = jsonfile.readFileSync(_filePath, JSON_OPTIONS);
}

function store(historyItem){
    historyItem.timestamp = Date.now();
    memCache.splice(0, 0, historyItem);
    jsonfile.writeFileSync(_filePath, memCache, JSON_OPTIONS);
}

function pull(){
    return _afterPull(memCache);
}

function pullWithFilter(needle){
    return  _afterPull(_filterContains(needle, pull()));
}

function _filterContains(searchFor, dataSet){
    var filtered = dataSet.map(function(item){
        var link = item.href,
            index = item.href.indexOf(searchFor);

        if (index >= 0){
            var end = index + searchFor.length;
            var prefix = link.substring(0, index),
                needle = link.substring(index, end),
                tail   = link.substring(end);

            return Object.assign({}, item, {
                prefix: prefix,
                needle: needle,
                tail: tail
            });
        }

        return null;
    }).filter(function(item){
        return item !== null;
    });

    return filtered;
}

function _afterPull(pullResult){
    return pullResult.map(function(item){
        var diff = _getTimeDiff(item.timestamp, Date.now());
        var diffText = _getReadableTimeDif(diff);
        return Object.assign({}, item, {
            time: diffText
        });
    });
}

function _getTimeDiff(timestampStart, timestampEnd){
    var tdiff = timediff(timestampStart, timestampEnd, 'DHmSs');
    return tdiff;
}

function _getReadableTimeDif(tdiff){
    if (tdiff.days > 0){
        return tdiff.days + (tdiff.days > 1 ? " days ago" : " day ago");
    }

    if (tdiff.hours > 0){
        return tdiff.hours + (tdiff.hours > 1 ? " hours ago" : " hour ago");
    }

    if (tdiff.minutes > 0){
        return tdiff.minutes + (tdiff.minutes > 1 ? " minutes ago" : " minute ago");
    }

    if (tdiff.seconds > 0){
        return tdiff.seconds + (tdiff.seconds > 1 ? " seconds ago" : " second ago");
    }

    if (tdiff.milliseconds > 0){
        return "less then a second ago";
    }

    return "some time ago";
}

module.exports = {
    init: init,
    store: store,
    pull: pull,
    pullWithFilter: pullWithFilter
};