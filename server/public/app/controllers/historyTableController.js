angular.module('SimilarWebTestTask')
    .controller('historyTableController', function ($scope, serverFetch) {
        if (serverData) {
            $scope.history = serverData;
        } else {
            serverFetch.fetch().success(function (data) {
                $scope.history = data;
            });
        }

        $scope.onSearchFilterChange = function(){
            //console.log($scope.searchFilter);
            $scope.fetchingInProgress = true;
            serverFetch.fetch($scope.searchFilter).success(function(data){
                $scope.history = data;
                $scope.fetchingInProgress = false;
            });
        }
    });