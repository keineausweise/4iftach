angular.module('SimilarWebTestTask')
    .service('serverFetch', function ($http) {

        function fetchAll() {
            return $http.get("/api/pull/");
        }

        function fetchByFilter(filter) {
            return $http.get("/api/pull/" + encodeURIComponent(filter));
        }

        this.fetch = function (filter) {
            if (filter) {
                return fetchByFilter(filter);
            } else {
                return fetchAll();
            }
        };

    });