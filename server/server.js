
'use strict';

var express = require('express');
var path = require('path');
var exphbs = require('express-handlebars');
var bodyParser = require('body-parser');

var apiStorrage = require('./api/storrage');
apiStorrage.init(path.join(__dirname, '/data/history.json'));

var app = express();

app.use(bodyParser.json());
var hbs = exphbs.create({
    defaultLayout: 'main',
    helpers: {
        "no-hbs": function (options) { return options.fn(); }
    }
});
app.engine('handlebars', hbs.engine);
app.set('view engine', 'handlebars');

app.get('/history', function (req, res) {
    res.render('history', {
        data: JSON.stringify(apiStorrage.pull())
    });
});

app.get('/', function (req, res) {
    res.status(302, "Moved permanently");
    res.header("Location", "/history");
    res.send();
});

app.get('/api/pull', function(req, res){
    res.header('Content-Type', 'application/json');
    res.send(JSON.stringify(apiStorrage.pull()));
});

app.get('/api/pull/:needle', function(req, res){
    var needle = req.params.needle;
    res.header('Content-Type', 'application/json');

    if (needle && needle.length > 1){
        var filtered = apiStorrage.pullWithFilter(req.params.needle);
        if (filtered.length == 0){
            res.header('X-Info', 'Filtering returned empty array');
            res.send('[]');
        }else{
            res.send(JSON.stringify(filtered));
        }
    } else {
        res.status(503, 'Wrong input. Needle lengths less then 2.');
    }
});

app.post('/api/store', function(req, res){
    apiStorrage.store(req.body);
    res.send({
        "stored": "ok"
    });
});

app.use(express.static('public'));

app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});
